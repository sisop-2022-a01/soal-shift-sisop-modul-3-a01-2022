#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <dirent.h>

pthread_t tid[4];
pid_t child;

void *mengunduh(void *arg){
	char *argv1[] = {"download", "-O", "music.zip", "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", NULL};
	char *argv2[] = {"download", "-O", "quote.zip", "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id, tid[0])){
		child=fork();
		if(child==0)
		    execv("/usr/bin/wget", argv1);
	}
	else if(pthread_equal(id, tid[1])){
		child=fork();
		if(child==0)
		    execv("/usr/bin/wget", argv2);
	}
	return NULL;
}

void *mengunzip(void *arg){
	char *argv1[] = {"unzip", "music.zip", "-d", "/home/poedanta/f1/music", NULL};
	char *argv2[] = {"unzip", "quote.zip", "-d", "/home/poedanta/f1/quote", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id, tid[2])){
		child=fork();
		if(child==0)
		    execv("/usr/bin/unzip", argv1);
	}
	else if(pthread_equal(id, tid[3])){
		child=fork();
		if(child==0)
		    execv("/usr/bin/unzip", argv2);
	}
	return NULL;
}

char *base64Decoder(char encoded[], int len_str){
	char *decoded_string;
	decoded_string=(char*)malloc(sizeof(char)*100);
	int i, j, k=0;
	int num=0;
	int count_bits=0;
	for(i=0; i<len_str; i+=4){
		num=0, count_bits=0;
		for(j=0; j<4; j++){
			if(encoded[i+j]!='='){
				num=num<<6;
				count_bits+=6;
			}
			if (encoded[i+j]>='A'&&encoded[i+j]<='Z')
				num=num|(encoded[i+j]-'A');
			else if(encoded[i+j]>='a'&&encoded[i+j]<='z')
				num=num|(encoded[i+j]-'a'+26);
			else if(encoded[i+j]>='0'&&encoded[i+j]<='9')
				num=num|(encoded[i+j]-'0'+52);
			else if(encoded[i+j]=='+')
				num=num|62;
			else if(encoded[i+j]=='/')
				num=num|63;
			else{
				num=num>>2;
				count_bits-=2;
			}
		}
		while(count_bits!=0){
			count_bits-=8;
			decoded_string[k++]=(num>>count_bits)&255;
		}
	}
	decoded_string[k]='\0';
	return decoded_string;
}

int main(){
	int i=0;
	int err;
	while(i<2){
		err=pthread_create(&(tid[i]), NULL, mengunduh, NULL);
		if(err!=0)
			printf("GA BISA :( : [%s]\n", strerror(err));
		else
			printf("TERHURA )':\n");
		i++;
	}
	while(i<4){
		sleep(5);
		err=pthread_create(&(tid[i]), NULL, mengunzip, NULL);
		if(err!=0)
			printf("GA BISA :( : [%s]\n", strerror(err));
		else
			printf("TERHURA )':\n");
		i++;
	}
	pthread_join(tid[0], NULL);
	pthread_join(tid[1], NULL);
	pthread_join(tid[2], NULL);
	pthread_join(tid[3], NULL);
	exit(0);
	return 0;
}
